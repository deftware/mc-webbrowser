# Minecraft Web Browser

A proof-of-concept fully functional web browser within Minecraft, as a Fabric mod.

## Demo

![Demo](https://gitlab.com/deftware/mc-webbrowser/raw/master/assets/demo.gif)

