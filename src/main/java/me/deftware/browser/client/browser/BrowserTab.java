package me.deftware.browser.client.browser;

import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.ScreenshotType;
import com.mojang.blaze3d.systems.RenderSystem;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

/**
 * @author Deftware
 */
public class BrowserTab {

    private final Logger logger = LogManager.getLogger("Window");

    @Getter
    private Page page;

    @Getter
    private BrowserContext context;

    @Getter
    private BrowserTexture texture;

    @Getter
    private boolean ready = false, polling = false, navigating = true;

    /**
     * Internal resolution
     */
    @Getter
    private int width, height, scrollHeight;

    private final ExecutorService thread;
    private final WebBrowser browser;
    private final String slug;

    @Getter
    private String url = "about:blank";

    private final List<Consumer<String>> onLoadCallbacks = new ArrayList<>();

    BrowserTab(WebBrowser browser, String slug, int width, int height) {
        this.browser = browser;
        this.thread = this.browser.getExecutor();
        this.slug = slug;
        this.width = width;
        this.height = height;
        this.logger.info("Creating new window with size {}x{}", width, height);
        this.context = this.browser.createContext(width, height);
        this.page = this.context.newPage();
        this.logger.info("Created context and page");
        this.init();
        RenderSystem.recordRenderCall(() -> {
            this.texture = new BrowserTexture();
        });
    }

    private void init() {
        this.page.onLoad(p -> {
            this.navigating = false;
            this.url = p.url();
            this.onLoadCallbacks.forEach(c -> c.accept(p.url()));
            this.getScrollAmount();
            this.poll();
        });
        this.page.onPageError(error -> {
            this.logger.debug("Something went wrong on {} {}", getUrl(), error);
        });
    }

    public void onLoad(Consumer<String> consumer) {
        this.onLoadCallbacks.add(consumer);
    }

    public synchronized void charTyped(char chr) {
        thread.submit(() -> {
            this.page.keyboard().down(String.valueOf(chr));
        });
    }

    public synchronized void mouse(double mouseX, double mouseY, NormalisedMouse runnable, boolean wait) {
        // Map to internal resolution
        double internalMouseX = this.width * mouseX;
        double internalMouseY = this.height * mouseY;
        // Run
        if (wait)
            this.navigating = true;
        thread.submit(() -> runnable.apply(internalMouseX, internalMouseY, this.page));
    }

    public synchronized void scroll(int amount) {
        thread.submit(() -> {
            this.page.evaluate(String.format("() => { window.scrollBy(0, %s); }", amount));
        });
    }

    private void getScrollAmount() {
        this.scrollHeight = (int) this.page.evaluate("() => { return document.body.scrollHeight; }");
    }

    public synchronized void navigate(String url) {
        this.navigating = true;
        if (!url.matches("http(s?)://"))
            url = "https://" + url;
        String realUrl = url;
        thread.submit(() -> this.page.navigate(realUrl));
    }

    public Optional<BufferedImage> screenshot() {
        try {
            byte[] bytes = this.page.screenshot(
                    new Page.ScreenshotOptions().setFullPage(false).setType(ScreenshotType.PNG)
            );
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytes));
            this.logger.debug("Got screenshot with size {}x{}", image.getWidth(), image.getHeight());
            return Optional.of(image);
        } catch (Exception ex) {
            logger.error("Failed to capture screenshot of {}", this.getUrl(), ex);
        }
        return Optional.empty();
    }

    public void poll() {
        if (polling || navigating)
            return;
        polling = true;
        thread.submit(() -> {
            Optional<BufferedImage> image = this.screenshot();
            if (image.isPresent()) {
                RenderSystem.recordRenderCall(() -> this.texture.upload(image.get()));
                this.polling = false;
                this.ready = true;
            } else {
                this.logger.error("Failed to update texture because of missing screenshot");
            }
        });
    }

    @FunctionalInterface
    public interface NormalisedMouse {

        void apply(double mouseX, double mouseY, Page page);

    }

}
