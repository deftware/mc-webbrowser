package me.deftware.browser.client.browser;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Playwright;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

/**
 * @author Deftware
 */
public class WebBrowser implements Runnable {

    private static int browserThreads = 0;

    private final Logger logger = LogManager.getLogger(WebBrowser.class.getSimpleName());

    private final Map<String, BrowserTab> tabs = new HashMap<>();

    @Getter
    private final ExecutorService executor;

    private final String threadName;

    private final Queue<Consumer<WebBrowser>> queue = new LinkedBlockingQueue<>();

    private BrowserType browserType;
    private Playwright playwright;
    private Browser browser;

    public WebBrowser() {
        this.threadName = WebBrowser.class.getSimpleName() + "_" + browserThreads++;
        this.executor = Executors.newSingleThreadExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setName(this.threadName);
            return thread;
        });
        this.executor.submit(this);
    }

    @Override
    public void run() {
        logger.info("Loading browser, this might take a while...");
        try {
            this.playwright = Playwright.create();
            this.browserType = this.playwright.firefox();
            this.browser = this.browserType.launch();
            logger.info("Loaded browser");
            queue.forEach(c -> c.accept(this));
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                logger.info("Shutting down browser...");
                browser.close();
            }));
        } catch (Exception ex) {
            logger.error("Failed to load browser", ex);
        }
    }

    public void onReady(Consumer<WebBrowser> consumer) {
        if (this.browser != null)
            this.executor.submit(() -> consumer.accept(this));
        else
            this.queue.add(consumer);
    }

    public BrowserContext createContext(int width, int height) {
        assertThread();
        return this.browser.newContext(
                new Browser.NewContextOptions()
                .setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")
                .setViewportSize(width, height)
                .setLocale("en-US")
        );
    }

    public BrowserTab getTab(String slug) {
        assertThread();
        if (this.tabs.containsKey(slug))
            return this.tabs.get(slug);
        BrowserTab tab = new BrowserTab(this, slug, 1920, 1080);
        this.tabs.put(slug, tab);
        return tab;
    }

    public boolean isOnThread() {
        return Thread.currentThread().getName().equalsIgnoreCase(this.threadName);
    }

    public void assertThread() {
        if (!isOnThread())
            throw new RuntimeException("Browser method called from the wrong thread!");
    }

}
