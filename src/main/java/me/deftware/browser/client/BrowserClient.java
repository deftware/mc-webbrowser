package me.deftware.browser.client;

import me.deftware.browser.client.browser.WebBrowser;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

/**
 * @author Deftware
 */
@Environment(EnvType.CLIENT)
public class BrowserClient implements ClientModInitializer {

    public static final WebBrowser browser = new WebBrowser();

    @Override
    public void onInitializeClient() { }

}
