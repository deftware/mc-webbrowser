package me.deftware.browser.client.gui;

import me.deftware.browser.client.BrowserClient;
import me.deftware.browser.client.browser.BrowserTab;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.util.Window;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Formatting;
import org.lwjgl.glfw.GLFW;

/**
 * @author Deftware
 */
public class GuiBrowser extends Screen {

    private TextFieldWidget searchBox;

    private BrowserView view;
    private BrowserTab tab;

    private final int searchWidth = 200, headerSize = 32, headerOffset = headerSize / 2 - 20 / 2;

    public GuiBrowser() {
        super(new LiteralText("Browser"));
        BrowserClient.browser.onReady(browser -> {
            this.tab = browser.getTab("tab");
            this.tab.onLoad(url -> {
                if (this.searchBox != null)
                    this.searchBox.setText(url);
            });
        });
    }

    @Override
    protected void init() {
        this.searchBox = new ShadowTextField(this.textRenderer, width / 2 - searchWidth / 2, headerOffset, searchWidth, 20, new LiteralText("Enter URL").formatted(Formatting.GRAY));
        this.children.add(this.searchBox);
        if (isLoaded()) {
            this.searchBox.setText(tab.getUrl());
            Window mcWindow = MinecraftClient.getInstance().getWindow();
            int scaledHeader = (int) (headerSize * mcWindow.getScaleFactor());
            int width = Math.min(this.tab.getWidth(), mcWindow.getWidth());
            int height = Math.min(this.tab.getHeight(), mcWindow.getHeight());
            this.view = new BrowserView(this.tab, 0, scaledHeader, width, height - scaledHeader);
            this.children.add(view);
        }
    }

    @Override
    public void mouseMoved(double mouseX, double mouseY) {
        if (this.view != null)
            this.view.mouseMoved(mouseX, mouseY);
    }

    @Override
    public void tick() {
        this.searchBox.tick();
        if (this.view != null)
            this.view.tick();
        else if (isLoaded())
            this.init();
    }

    @Override
    public boolean charTyped(char chr, int modifiers) {
        if (this.searchBox.isFocused()) {
            this.searchBox.charTyped(chr, modifiers);
            return true;
        }
        return false;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        if (keyCode == GLFW.GLFW_KEY_ENTER && isLoaded()) {
            this.tab.navigate(this.searchBox.getText());
            return true;
        }
        return super.keyPressed(keyCode, scanCode, modifiers);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        this.renderBackground(matrices);
        this.searchBox.render(matrices, mouseX, mouseY, delta);
        if (!isLoaded() || this.tab.isNavigating())
            this.textRenderer.drawWithShadow(matrices, "Loading...", width / 2f + searchBox.getWidth() / 2f + 10, headerSize / 2f - textRenderer.fontHeight / 2f, 0xFFFFFF);
        if (isLoaded() && tab.isReady()) {
           this.view.render();
        }
    }

    private boolean isLoaded() {
        return this.tab != null;
    }

}
