package me.deftware.browser.client.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import me.deftware.browser.client.browser.BrowserTab;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.Element;
import net.minecraft.client.util.Window;

/**
 * @author Deftware
 */
public class BrowserView implements Element {

    private final BrowserTab tab;
    private final int x, y, width, height;
    private boolean focused = false;

    private final Window mcWindow = MinecraftClient.getInstance().getWindow();

    /**
     * Update texture every 50 ms
     */
    private long pollingRate = 50, lastPoll = 0;

    public BrowserView(BrowserTab tab, int x, int y, int width, int height) {
        this.tab = tab;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public boolean mouseScrolled(double mouseX, double mouseY, double amount) {
        mouseX *= mcWindow.getScaleFactor();
        mouseY *= mcWindow.getScaleFactor();
        if (withinBounds(mouseX, mouseY)) {
            this.tab.scroll(
                    (int) (-amount * 100)
            );
            return true;
        }
        return false;
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        mouseX *= mcWindow.getScaleFactor();
        mouseY *= mcWindow.getScaleFactor();
        if (withinBounds(mouseX, mouseY)) {
            this.focused = true;
            mouseX = (mouseX - x) / width;
            mouseY = (mouseY - y) / height;
            this.tab.mouse(mouseX, mouseY, (x, y, page) -> {
                page.mouse().click(x, y);
            }, false);
            return true;
        } else {
            this.focused = false;
            return false;
        }
    }

    @Override
    public boolean isMouseOver(double mouseX, double mouseY) {
        mouseX *= mcWindow.getScaleFactor();
        mouseY *= mcWindow.getScaleFactor();
        return withinBounds(mouseX, mouseY);
    }

    private boolean withinBounds(double mouseX, double mouseY) {
        return mouseX > x && mouseX < x + width && mouseY > y && mouseY < y + height;
    }

    @Override
    public void mouseMoved(double mouseX, double mouseY) {
        mouseX *= mcWindow.getScaleFactor();
        mouseY *= mcWindow.getScaleFactor();
        if (withinBounds(mouseX, mouseY)) {
            mouseX = (mouseX - x) / width;
            mouseY = (mouseY - y) / height;
            this.tab.mouse(mouseX, mouseY, (x, y, page) -> {
                page.mouse().move(x, y);
            }, false);
        }
    }

    @Override
    public boolean charTyped(char chr, int modifiers) {
        if (this.focused) {
            this.tab.charTyped(chr);
            return true;
        }
        return false;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        if (this.focused) {
            this.tab.charTyped((char) keyCode);
        }
        return false;
    }

    public void render() {
        if (tab.isReady()) {
            Window window = MinecraftClient.getInstance().getWindow();
            this.customMatrix(window);
            this.tab.getTexture().bind();
            this.tab.getTexture().draw(x, y, width, height, 0, 0, tab.getWidth(), tab.getHeight());
            this.minecraftMatrix(window);
        }
    }

    public void tick() {
        if (tab.isReady() && !tab.isPolling() && lastPoll + pollingRate < System.currentTimeMillis()) {
            lastPoll = System.currentTimeMillis();
            this.tab.poll();
        }
    }

    protected void minecraftMatrix(Window window) {
        setMatrix(
                (float) (window.getFramebufferWidth() / window.getScaleFactor()),
                (float) (window.getFramebufferHeight() / window.getScaleFactor())
        );
    }

    protected void customMatrix(Window window) {
        setMatrix(
                (float) window.getWidth(),
                (float) window.getHeight()
        );
    }

    protected static void setMatrix(float width, float height) {
        RenderSystem.matrixMode(5889);
        RenderSystem.loadIdentity();
        RenderSystem.ortho(0.0D, width, height, 0.0D, 1000.0D, 3000.0D);
        RenderSystem.matrixMode(5888);
        RenderSystem.loadIdentity();
        RenderSystem.translatef(0.0F, 0.0F, -2000.0F);
        RenderSystem.clear(256, MinecraftClient.IS_SYSTEM_MAC);
    }

}
